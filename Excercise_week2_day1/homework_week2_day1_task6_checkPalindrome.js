const word = process.argv[2];

let isPalindrome = true;
let length = word.length;

if (length <= 1) {
    isPalindrome = true;
} else {
    const lastIndex = length - 1;
    for (let i = 0, j = lastIndex; i <= lastIndex || j >= 0; i++, j--) {
        if (word[i] !== word[j]) {
            isPalindrome = false;
            break;
        }
    }    
}

if (isPalindrome) {
    console.log(`Yes, \'${word}\' is a palindrome`)
} else {
    console.log(`No, \'${word}\' is not a palindrome`)
}