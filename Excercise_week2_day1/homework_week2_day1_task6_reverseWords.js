const wordsAsString = process.argv[2];
const words = wordsAsString.split(" ");
let reverse = "";
for (let i = 0; i < words.length; i++) {
    const word = words[i];
    const reverseWord = [...word].reverse().join("");
    reverse = reverse + reverseWord + " "
}

console.log(reverse);