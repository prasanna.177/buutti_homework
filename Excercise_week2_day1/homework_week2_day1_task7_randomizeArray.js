const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

for (let i = 0; i < array.length; i++) {
    const randomIndex = Math.floor(Math.random() * i);
    const temp = array[i];
    array[randomIndex] = array[i];
    array[i] = temp;
}

console.log(array);