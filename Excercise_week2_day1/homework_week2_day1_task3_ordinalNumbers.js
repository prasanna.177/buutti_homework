const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ['st', 'nd', 'rd', 'th'];
let array =[];
for (let i = 0; i < competitors.length-1; i++){
    let ordinal = ordinals[i];
    if (i >= 3) {
        ordinal = ordinals[3];
    }
    const element = (i+1)+ ordinal + " competitor was " + competitors[i];
    array.push (element);
}
console.log(array);