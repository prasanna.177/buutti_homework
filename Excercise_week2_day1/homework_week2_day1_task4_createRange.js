let start = parseInt(process.argv[2]);
let end = parseInt(process.argv[3]);

let array = [];

let isStartGreaterThanEnd = false;
if (start > end) {
    // swap
    const temp = start;
    start = end;
    end = temp;
    isStartGreaterThanEnd = true;
}

for (let i = start; i <= end; i++) {
    array.push(i);
}

if (isStartGreaterThanEnd) {
    console.log(array.reverse());
} else {
    console.log(array);
}
