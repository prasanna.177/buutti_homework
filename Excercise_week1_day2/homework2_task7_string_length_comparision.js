const name1 = process.argv[2];
const name2 = process.argv[3];
const name3 = process.argv[4];

function compare(a, b){
    if (a.length > b.length) {
        return -1;
    } else if (a.length < b.length) {
        return 1;
    } else {
        return 0;
    }
}
const array = [name1, name2, name3].sort(compare);

let str = "";
for (let i = 0; i < array.length; i++) {
    str = str + array[i] + " ";
}
console.log(str);
