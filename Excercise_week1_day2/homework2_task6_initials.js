// node homework2_task6_initials.js Jake Junior Mike
const name1 = process.argv[2];
const name2 = process.argv[3];
const name3 = process.argv[4];

const initials = name1.charAt(0).toLowerCase() + "." + name2.charAt(0).toLowerCase() + "." + name3.charAt(0).toLowerCase();
console.log(initials);